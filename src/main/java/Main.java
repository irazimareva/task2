import clients.Client;
import clients.Recipient;
import clients.Sender;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import transfer.Currency;
import transfer.Transfer;
import transfer.TransferFromAccountToAccount;
import transfer.TransferFromCardToCard;

public class Main {

    public static void main(String[] args) throws JsonProcessingException {
        Client recipient = new Recipient("Petrov", "Petr", "Petrovich", 10, 11, 123.00);
        Client sender = new Sender("Ivanov", "Ivan", "Ivanovich", 20, 21, 321.00);
        System.out.println("Lower case    " + recipient.printLowerCase());
        System.out.println("Upper case    " + sender.printUpperCase());

        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(sender);
        System.out.println("\n Sender before transaction " + json);

        json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(recipient);
        System.out.println("Recipient before transaction " + json);

        System.out.println("---------------- Do transaction from card to card ----------------");
        Transfer transferFromCardToCard = new TransferFromCardToCard();
        transferFromCardToCard.transaction(1,10, Currency.RUBLE, recipient, sender);
        System.out.println(transferFromCardToCard.print());

        System.out.println("\n Amount of money. Sender. After transaction " + transferFromCardToCard.getSender().getAmount());
        System.out.println("Amount of money. Recipient. After transaction " + transferFromCardToCard.getRecipient().getAmount());


        json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(transferFromCardToCard);
        System.out.println("\n Transaction JSON " + json);


        System.out.println("---------------- Do transaction from account to account ----------------");
        Transfer transferFromAccountToAccount = new TransferFromAccountToAccount();
        transferFromAccountToAccount.transaction(2,80, Currency.RUBLE, recipient, sender);
        System.out.println(transferFromAccountToAccount.print());

        System.out.println("\n Amount of money. Sender. After transaction " + transferFromAccountToAccount.getSender().getAmount());
        System.out.println("Amount of money. Recipient. After transaction " + transferFromAccountToAccount.getRecipient().getAmount());

        System.out.println("\n Throw exception. Illegal amount ");
        transferFromCardToCard.transaction(2,500,Currency.RUBLE, recipient, sender);
    }
}
