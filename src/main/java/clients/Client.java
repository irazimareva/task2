package clients;

import java.util.Objects;

public abstract class Client {
    private String lastname;
    private String firstname;
    private String secondname;
    private long idOfCard;
    private long idOfAccount;
    private double amount;

    public Client(String lastname, String firstname, String secondname, long idOfCard, long idOfAccount, double amount) {
        this.lastname = lastname;
        this.firstname = firstname;
        this.secondname = secondname;
        this.idOfCard = idOfCard;
        this.idOfAccount = idOfAccount;
        this.amount = amount;
    }

    public Client() {
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSecondname() {
        return secondname;
    }

    public void setSecondname(String secondname) {
        this.secondname = secondname;
    }

    public long getIdOfCard() {
        return idOfCard;
    }

    public void setIdOfCard(long idOfCard) {
        this.idOfCard = idOfCard;
    }

    public long getIdOfAccount() {
        return idOfAccount;
    }

    public void setIdOfAccount(long idOfAccount) {
        this.idOfAccount = idOfAccount;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String printLowerCase (){
        String strinToPrint = (lastname + " " + firstname + " " + secondname);
        return strinToPrint.toLowerCase();
    }

    public String printUpperCase (){
        String strinToPrint = (lastname + " " + firstname + " " + secondname);
        return strinToPrint.toUpperCase();
    }

    @Override
    public String toString() {
        return "Client{" +
                "lastname='" + lastname + '\'' +
                ", firstname='" + firstname + '\'' +
                ", secondname='" + secondname + '\'' +
                ", idOfCard=" + idOfCard +
                ", idOfAccount=" + idOfAccount +
                ", amount=" + amount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return idOfCard == client.idOfCard && idOfAccount == client.idOfAccount && Double.compare(client.amount, amount) == 0 && Objects.equals(lastname, client.lastname) && Objects.equals(firstname, client.firstname) && Objects.equals(secondname, client.secondname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lastname, firstname, secondname, idOfCard, idOfAccount, amount);
    }
}
