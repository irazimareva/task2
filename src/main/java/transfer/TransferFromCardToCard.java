package transfer;

public class TransferFromCardToCard extends Transfer {
    @Override
    public String  print (){
        return "ПЕРЕВОД С КАРТЫ НА КАРТУ: " + "\n" +
                "Номер перевода: " + super.getIdTransfer() + "\n" +
                "Дата: " + super.getDateOfTransfer() + "\n" +
                "Номер карты получателя: " + super.getRecipient().getIdOfCard() + "\n" +
                "Номер карты отправителя: " + super.getSender().getIdOfCard() + "\n" +
                "Валюта: " + super.getCurrency() + "\n" +
                "Сумма: " + super.getAmount() + "\n" +
                "Получатель: " + super.getRecipient().printUpperCase() + "\n" +
                "Отправитель: " + super.getSender().printUpperCase();
    }
}
