package transfer;

public class TransferFromAccountToAccount extends Transfer {
    @Override
    public String  print (){
        return "ПЕРЕВОД СО СЧЕТА НА СЧЕТ: " + "\n" +
                "Номер перевода: " + super.getIdTransfer() + "\n" +
                "Дата: " + super.getDateOfTransfer() + "\n" +
                "Номер счета получателя: " + super.getRecipient().getIdOfAccount() + "\n" +
                "Номер счета отправителя: " + super.getSender().getIdOfAccount() + "\n" +
                "Валюта: " + super.getCurrency() + "\n" +
                "Сумма: " + super.getAmount() + "\n" +
                "Получатель: " + super.getRecipient().printUpperCase() + "\n" +
                "Отправитель: " + super.getSender().printUpperCase();
    }
}
