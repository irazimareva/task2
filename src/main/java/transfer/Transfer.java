package transfer;

import clients.Client;
import clients.Recipient;
import clients.Sender;
import exception.NoSuchMoneyException;

import java.util.Date;
import java.util.Objects;

public class Transfer {
    private long idTransfer;
    private double amount;
    private Currency currency;
    private Client recipient;
    private Client sender;
    private Date dateOfTransfer;

    /*public Transfer(long idTransfer, double amount, Currency currency, Recipient recipient, Sender sender, Date dateOfTransfer) {
        this.idTransfer = idTransfer;
        this.amount = amount;
        this.currency = currency;
        this.recipient = recipient;
        this.sender = sender;
        this.dateOfTransfer = dateOfTransfer;
    }*/

    public Transfer() {
    }

    public long getIdTransfer() {
        return idTransfer;
    }

    public void setIdTransfer(long idTransfer) {
        this.idTransfer = idTransfer;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Client getRecipient() {
        return recipient;
    }

    public void setRecipient(Client recipient) {
        this.recipient = recipient;
    }

    public Client getSender() {
        return sender;
    }

    public void setSender(Client sender) {
        this.sender = sender;
    }

    public Date getDateOfTransfer() {
        return dateOfTransfer;
    }

    public void setDateOfTransfer(Date dateOfTransfer) {
        this.dateOfTransfer = dateOfTransfer;
    }

    public String print (){
        return null;
    }

    public void transaction (long idTransfer, double amount, Currency currency, Client recipient, Client sender)
            throws NoSuchMoneyException {
        if (sender.getAmount() < amount) throw new NoSuchMoneyException("Перевод совершить нельзя, недостаточно средств");
        else {
            this.idTransfer = idTransfer;
            this.amount = amount;
            this.currency = currency;
            this.recipient = recipient;
            this.sender = sender;
            this.dateOfTransfer = new Date();

            sender.setAmount(sender.getAmount() - amount);
            recipient.setAmount(recipient.getAmount() + amount);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transfer transfer = (Transfer) o;
        return idTransfer == transfer.idTransfer && Double.compare(transfer.amount, amount) == 0 && currency == transfer.currency && Objects.equals(recipient, transfer.recipient) && Objects.equals(sender, transfer.sender) && Objects.equals(dateOfTransfer, transfer.dateOfTransfer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idTransfer, amount, currency, recipient, sender, dateOfTransfer);
    }
}
